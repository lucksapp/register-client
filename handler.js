/* 
 * recebe informação do app electron
 * 
 * verifica se o thingName já está em uso
 * verifica se o refer digitado existe
 * 
 * cria/pega o thingGroup com o nome do refer
 * 
 * cria um certificado para o thingName
 * associa policy ao certificado
 * 
 * cria o thing com thingName
 * associa certificado ao thing
 * associa o thingGroup (refer) ao thingName
 * 
 * cria um shadow em branco para o thing
 * 
 * cria um item no sql para o thing

*/


const AWS = require('aws-sdk');
const iot = new AWS.Iot();
const request = require("request");

const lambda = new AWS.Lambda({  
  region: "us-east-1"
});

getContent = function(url) {
	// return new pending promise
	return new Promise((resolve, reject) => {
	  // select http or https module, depending on reqested url
	  const lib = url.startsWith('https') ? require('https') : require('http');
	  const request = lib.get(url, (response) => {
		// handle http errors
		if (response.statusCode < 200 || response.statusCode > 299) {
		   reject(new Error('Failed to load page, status code: ' + response.statusCode));
		 }
		// temporary data holder
		const body = [];
		// on every content chunk, push it to the data array
		response.on('data', (chunk) => body.push(chunk));
		// we are done, resolve promise with those joined chunks
		response.on('end', () => resolve(body.join('')));
	  });
	  // handle connection errors of the request
	  request.on('error', (err) => reject(err))
	  })
}

validateRefer = async function(refer){
	if (refer == ''){
        return false;
	}

	try {
		const url = 'https://s3.amazonaws.com/lucksapp-public/institutions.json';
		const inst = await getContent(url);
		const some = JSON.parse(inst).institutions.some((v)=>{
			if (refer === v.name){
				return true;
			}
		});
		if (some){
			return true;
		}
	} catch(e){
		console.error("Erro fatal! get institution list: ", e);
	}

	try{
		await iot.describeThing({thingName: refer}).promise();
		return true;
	} catch(e){
		if (!e.code == "ResourceNotFoundException"){
			console.error("Erro fatal! validate refer describe thing: ", e);
		}
	}
	return false;
}


validateName = async function(name){

	try {
		const url = 'https://s3.amazonaws.com/lucksapp-public/institutions.json';
		const inst = await getContent(url);
		const some = JSON.parse(inst).institutions.some((v)=>{
			if (name === v.name){
				return true;
			}
		});
		if (some){
			return false;
		}
	} catch(e){
		console.error("Erro fatal! get institution list: ", e);
	}

	try{
		await iot.describeThing({thingName: name}).promise();
		return false;
	} catch(e){
		return true;
	}

}

createCertificate = async function(){

	let certificate;

	try {
		certificate = await iot.createKeysAndCertificate({setAsActive: true}).promise();
	} catch(e){
		console.error("Erro fatal! create certificate: ", e);
		return false;
	}

	try {
		await iot.attachPolicy({"policyName": 'Lucksapp', "target": certificate.certificateArn}).promise();
	} catch(e){
		console.error("Erro fatal! attach policy certificate: ", e);
		return false;
	}

	return certificate;
}

createThing = async function(name, certificate){
	console.log(name);
	try {
		await iot.createThing({thingName: name}).promise();
	} catch(e){
		console.error("Erro fatal! create thing: ", e);
		return false;
	}
	
	try {
		await iot.attachThingPrincipal({principal: certificate.certificateArn, thingName: name}).promise();
	} catch(e){
		console.error("Erro fatal! attach thing: ", e);
		return false;
	}
	return true;
}
	
getEndpoint = async function(name){
	let endpoint;
	try {
		const d = await iot.describeEndpoint({}).promise();
		endpoint = d.endpointAddress;
	} catch(e){
		console.error("Erro fatal! describe endpoint: ", e);
		return false;
	}

	return endpoint;
}

createThingShadow = async function(name, endpoint){
	// shadow em branco
	const shadow = {
			state: {
					desired: {
								wallet: '',
								paymentMade: false,
								badWallet: false,
								prizes: []
					}
			}
	}
		
	const iotdata = new AWS.IotData({endpoint: endpoint});
	
	// cria o shadow no thing
	try {
		iotdata.updateThingShadow({thingName: name, payload: JSON.stringify(shadow)}).promise();
	} catch (e){
		console.error("Erro fatal! update thing shadow: ", e);
		return false;
	}		
	
	return true;
}

insertThingDatabase = async function(name, refer, info){
	const date = new Date().toJSON().slice(0,10);
														
	let query = {
		type:'insert',
		table:'things',
		values:{
			"thing_name": name,
			"refer": refer,
			"created_at": date,
		}
	};							
	query.values = Object.assign({}, query.values, JSON.parse(info));
	
	const params = {
		FunctionName: 'db-handler-dev-db_handler',
		Payload: JSON.stringify(query)
	};

	try {
		await lambda.invoke(params).promise();
	} catch (e){
		console.error("Erro fatal! insert db: ", e);
		return false;
	}
	
	return true;
}

const success = (name, data, refer, endpoint) => {
	let res = obj(200);
	
	res.body = JSON.stringify({
		thingName: name,
		refer: refer,
		certificatePem: data.certificatePem,
		privateKey: data.keyPair.PrivateKey,
		endpoint: endpoint
	  })

	return res;
  };

  const obj = (code) => {
	return {
		statusCode: code,
		headers: {
		  'Access-Control-Allow-Origin': '*'
		}
	  };	
  };
  
  const invalid_name_reponse = () => {
	  return obj(406);
  };
    
  const invalid_refer_response = () => {
	  return obj(409);
  };
  
  const unknown_response = () => {
	  return obj(400);
  };
  

module.exports.auth = (event, context, callback) => {
	context.callbackWaitsForEmptyEventLoop = false;

	void async function(){

		const request = JSON.parse(event.body);
		const refer = request.refer;
		const name = request.name;
		const info = request.info;

		const a = await validateRefer(refer);
		if (!a){
			return callback(null, invalid_refer_response());
		}

		const b = await validateName(name);
		if (!b){
			return callback(null, invalid_name_reponse());
		}

		const certificate = await createCertificate();
		if (!certificate){
			return callback(null, unknown_response());
		}

		const c = await createThing(name, certificate);
		if (!c){
			return callback(null, unknown_response());
		}

		const endpoint = await getEndpoint();
		if (!endpoint){
			return callback(null, unknown_response());
		}

		const d = await createThingShadow(name, endpoint);
		if (!d){
			return callback(null, unknown_response());
		}

		const e = await insertThingDatabase(name, refer, info);
		if (!e){
			return callback(null, unknown_response());
		}

		return callback(null, success(name, certificate, refer, endpoint));
		
	}();
}

/*
module.exports.auth = (event, context, callback) => {
	context.callbackWaitsForEmptyEventLoop = false;
		
	var body = JSON.parse(event.body);
	var cert = '';
	var endpoint = '';
	var refer = body.refer;
	if (refer == ''){
        return callback(null, buildReferInvalidObject());
	}
	/*
	if (body.recaptcha == '' || body.recaptcha == 'null') {
        return callback(null, buildRecaptchaInvalidObject());
	}
    const recaptchaVerif = {
        uri: "https://www.google.com/recaptcha/api/siteverify",
        json: true,
        form: {
            secret: '6Lev51wUAAAAAIJqdiQ8bLs0tbCi1d3hGIbjlHIB',
            response: body.recaptcha
        }
	};

	// primeiro verifica o recaptcha
	request.post(recaptchaVerif, function (err, response, b) {
		if (err) {
			return callback(null, buildRecaptchaInvalidObject());
		}

		if (!b.success) {
			return callback(null, buildRecaptchaInvalidObject());
		}

		// recaptcha confirmado	
		iot.describeEndpoint({}, (err, data) => {
			if (err) return callback(err);
			endpoint = data.endpointAddress;
		});
			
		// primeiro passo:
		// verifica se o thingName escolhido já está em uso
		iot.describeThing({thingName: body.name}, function(err, data) {
		if (err){
				if (err.code == 'ResourceNotFoundException'){ //thingName disponível
						
						// verifica se refer ja existe
						iot.describeThing({thingName: refer}, function(err, data) {
							if (err){
								// refer nao existe, cancela cadastro do thing
								callback(null, buildReferInvalidObject());
								return;
							}
							
							iot.describeThingGroup({thingGroupName: refer}, (e, d) => {
								if (e) {
									// refer nunca foi indicado com refer
									// criar thing group com nome dele
									iot.createThingGroup({thingGroupName: refer}, (e, d) => {
										if (e){
											console.log('nao foi possivel criar thing group no nome ', refer);
											console.log(e);
											callback(e, buildBadObject());
											return;							
										}
									});
								}
							});
										
							// criar certificado
							iot.createKeysAndCertificate({setAsActive: true}, function(err, certificate) {
											cert = certificate;
																
								// relaciona policy e certificado
								iot.attachPolicy({"policyName": 'Lucksapp',"target": certificate.certificateArn}, (err, data) => {
										//Ignore if the policy is already attached
										if (err && (!err.code || err.code !== 'ResourceAlreadyExistsException')) {
											console.log('Erro ao attach certificado ao policy', err);
											callback(err, data);
											return;
										}
									});				
												
								// criar o thing
								iot.createThing({thingName: body.name}, function(err, data) {
								if (err){ // erro desconhecido
										console.log('Thing nao foi criado', err);
										callback(err, buildBadObject());
										return;
								}
								else {					  
									// attach certificado ao thing
									iot.attachThingPrincipal({principal: cert.certificateArn, thingName: data.thingName}, function(err, data) {
											if (err){
												console.log("Thing nao recebeu certificado", err);
												callback(err, buildBadObject());
												return;
											}			
											
											// attach thing ao thingGroup
											var p = {thingGroupsToAdd: [refer], thingName: body.name};
											iot.updateThingGroupsForThing(p, (e,d) => {
												if (e){
													console.log("Thing group nao foi associado", e);
													callback(err, buildBadObject());
													return;
												}
												
												// pegar endpoint
												iot.describeEndpoint({}, (e, d) => {
													if (e){
														console.log("Erro ao pegar endpoint ", e);
														callback(err, buildBadObject());
														return;
													}
													var endpoint = d.endpointAddress;
													
													// shadow em branco
													var shadow = {
															state: {
																	desired: {
																				wallet: '',
																				paymentMade: false,
																				badWallet: false,
																				prizes: []
																	}
															}
													}
													
													// chama a interface com os dados iot
													var iotdata = new AWS.IotData({endpoint: endpoint});
													
													// cria o shadow no thing
													iotdata.updateThingShadow({thingName: body.name, payload: JSON.stringify(shadow)}, (e, d) => {
														if (e) {
															console.log("Erro ao update shadow ", e);
															callback(err, buildBadObject());
															return;
														}
														
														// inserir no banco de dados dynamodb
														
														const date = new Date().toJSON().slice(0,10).replace(/-/g,'/');
																											
														let query = {
															type:'insert',
															table:'things',
															values:{
																"thing_name": body.name,
																"refer": refer,
																"created_at": date,
															}
														};							
														query.values = Object.assign({}, query.values, JSON.parse(body.info));
														
														var params = {
															FunctionName: 'db-handler-dev-db_handler',
															Payload: JSON.stringify(query)
														};
														lambda.invoke(params, function(err, data) {
															if (err){
																console.log("Erro em insert db ", err);
																callback(err, buildBadObject());
															}
															else{
																callback(null, buildResponseObject(body.name, cert, refer, endpoint));
															}
														});
														
															
																																			
													});
												});
											});								
																
									});
								}
								});
							});
						
						
						});
				}
				else{
					console.error('DescribeThing deu erro', err); // unexpected error
					callback(null, buildBadObject());
				}
		} // nome de usuario ja registrado
		else     callback(null, buildErrorObject());
		});

	//});
};

const buildResponseObject = (name, data, refer, endpoint) => {
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },  
    body: JSON.stringify({
	  thingName: name,
      certificateArn: data.certificateArn,
      certificateId: data.certificateId,
      certificatePem: data.certificatePem,
      publicKey: data.keyPair.PublicKey,
      privateKey: data.keyPair.PrivateKey,
      refer: refer,
      endpoint: endpoint
    })
  };
};

const buildErrorObject = () => {
  return {
    statusCode: 406,
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  };
};
/*
const buildRecaptchaInvalidObject = () => {
	return {
		statusCode: 412,
		headers: {
			'Access-Control-Allow-Origin': '*'
		}
	};
};

const buildReferInvalidObject = () => {
	return {
		statusCode: 409,
		headers: {
			'Access-Control-Allow-Origin': '*'
		}
	};
};

// erros desconhecidos
const buildBadObject = () => {
  return {
    statusCode: 400,
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  };
};*/
